# White Sox Ranked Prospect List

This loads a CSV of prospects. The rows can be moved up or down. When moved the corresponding group, rank and number will update to reflect the new order. Working online at [https://brettphillips.tech/rankedlist/](https://brettphillips.tech/rankedlist/)

## Demo

### Mobile

**Note**: This is an example captured on a desktop view of mobile. Use a mobile device will allow to scroll down the list with a selected row.

![Mobile view](mobile.gif "Mobile view")

### Desktop

![Desktop view](desktop.gif "Desktop view")

## Technical Details

In addition to using the Vue framework, I leveraged a few packages to aid in the development. 

### [Papa Parse](https://www.papaparse.com/)

An in-browser CSV parser takes care of cases and different browsers

### [Vue Draggable](https://github.com/SortableJS/Vue.Draggable)

A robust framework to make HTML elements draggable.

### [Bulma](http://bulma.io/)

A modular, responsive CSS framework that styles most the page. 

### [FontAwesome](https://fontawesome.com/)

Adding the helpful images to move the columns up and down on a tablet or mobile device

## Going Further

Some ideas I had to add some functionality in the future:

- Allow columns to be hidden or moved
- Add API to retrieve the data and send updates when changes made. 
- Click on prospect to update row information such as rank or new remarks 
- Would want to talk with people using the tool to get an idea of a better way to design for mobile display. There is a lot of information and I imagine there are only key pieces useful while scrolling through and could show more information if selected.  

## Project setup

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).